//
//  iOS_Development_TaskTests.swift
//  iOS Development TaskTests
//
//  Created by Martin Markov on 9/7/16.
//  Copyright © 2016 Appolica. All rights reserved.
//

import XCTest
@testable import iOS_Development_Task

class iOS_Development_TaskTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testQueryEncode() {
        XCTAssertEqual("Michael Jackson".queryEncode(), "Michael+Jackson")
        XCTAssertEqual("Michael    Jackson     2000".queryEncode(), "Michael+Jackson+2000")
        XCTAssertEqual("".queryEncode(), "")
        XCTAssertEqual("Michael".queryEncode(), "Michael")
    }
}
