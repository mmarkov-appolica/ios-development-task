# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

This is an iOS project written in Swift. The app is running on:

* version: iOS 8.0+
* devices: iPhone/iPad (Universal App)
* device orientations: Portrait/Landscape

All requirements and bonus credits for this task are implemented in the project:

* support of previous iOS versions
**  If i was supporting only 9.0+ I would use Stack view for the layout of the details view of the album.
* no cocoapods and libraries used
* there is a test for one of the functions of the project
* universal app
* the web query and JSON parsing are done on the background thread

Design decisions:

* I chose to use UISplitViewController because it has support for both phone and tablet and is easy for use to implement the current task.

Things could have done if have more time and improvements:

* use libraries (Alamofire, SwiftyJSON etc.)
* handle + sign in the text field so it wont be send in the parameters of the request
* all strings should be in localized files
* if code is more complex it will be good to put more comments
* loading and error states of the list screen