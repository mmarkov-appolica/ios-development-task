//
//  AlbumsResponseModel.swift
//  iOS Development Task
//
//  Created by Martin Markov on 9/7/16.
//  Copyright © 2016 Appolica. All rights reserved.
//

import Foundation

public class AlbumsResponseModel : Mappable {
    
    var count: Int?
    var albums: [AlbumModel]?
    
    required public init(dictionary: [String : AnyObject]) {
        count = dictionary["resultCount"] as? Int
        if let albumsDictionaries = dictionary["results"] as? [[String: AnyObject]] {
            albums = albumsDictionaries.map { AlbumModel(dictionary: $0) }
        }
    }
    
}
