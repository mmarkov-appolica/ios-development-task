//
//  Mappable.swift
//  iOS Development Task
//
//  Created by Martin Markov on 9/7/16.
//  Copyright © 2016 Appolica. All rights reserved.
//

import Foundation

protocol Mappable {
    
    init(dictionary: [String: AnyObject])
    
}