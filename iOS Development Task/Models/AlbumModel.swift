//
//  AlbumModel.swift
//  iOS Development Task
//
//  Created by Martin Markov on 9/7/16.
//  Copyright © 2016 Appolica. All rights reserved.
//

import Foundation

public class AlbumModel: Mappable {
    
    var artistName: String?
    var trackName: String?
    var albumName: String?
    var price: Double?
    var currency: String?
    var releaseDate: NSDate?
    var thumbnailUrl: String?
    
    
    required public init(dictionary: [String : AnyObject]) {
        artistName = dictionary["artistName"] as? String
        trackName = dictionary["trackName"] as? String
        albumName = dictionary["collectionName"] as? String
        price = dictionary["trackPrice"] as? Double
        currency = dictionary["currency"] as? String
        thumbnailUrl = dictionary["artworkUrl100"] as? String
        
        if let date = dictionary["releaseDate"] as? String {
            releaseDate = NSDate.dateFromString(date)
        }
        
    }
    
}

extension AlbumModel {
    
    var priceLabel: String? {
        guard let price = price, currency = currency else {
            return nil
        }
        
        return "\(price) \(currency)"
    }
    
}