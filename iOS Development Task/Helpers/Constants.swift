//
//  Constants.swift
//  iOS Development Task
//
//  Created by Martin Markov on 9/7/16.
//  Copyright © 2016 Appolica. All rights reserved.
//

import Foundation

struct Image {
    static let NoImage = "100px-No_image_available.svg"
}