//
//  AlbumsFetcher.swift
//  iOS Development Task
//
//  Created by Martin Markov on 9/7/16.
//  Copyright © 2016 Appolica. All rights reserved.
//

import Foundation

public class AlbumsFetcher {
    
    private var baseURL = "http://itunes.apple.com/"
    private var session: NSURLSession
    private var currentTask: NSURLSessionDataTask?
    
    init() {
        let sessionConfiguration = NSURLSessionConfiguration.defaultSessionConfiguration()
        sessionConfiguration.HTTPMaximumConnectionsPerHost = 1
        
        session = NSURLSession(configuration: sessionConfiguration, delegate: nil, delegateQueue: nil)
    }
    
    public func getAlbums(query: String, completion: ([AlbumModel]?, NSError?) -> Void) {
        let path = "search"
        let encodedQuery = query.queryEncode()
        let urlString = baseURL + path + "?term=" + encodedQuery
        
        if let url = NSURL(string: urlString) {
            currentTask = session.dataTaskWithURL(url, completionHandler: { (data, response, error) in
                if let error = error {
                    dispatch_async(dispatch_get_main_queue(), {
                        completion(nil, error)
                    })
                } else if let data = data {
                    do {
                        let jsonDictionary = try NSJSONSerialization.JSONObjectWithData(data, options: .MutableContainers) as! [String: AnyObject]
                        let albumsResponse = AlbumsResponseModel(dictionary: jsonDictionary)
                        
                        dispatch_async(dispatch_get_main_queue(), {
                            completion(albumsResponse.albums, nil)
                        })
                    } catch let parseError as NSError {
                        dispatch_async(dispatch_get_main_queue(), {
                            completion(nil, parseError)
                        })
                    }
                } else {
                    dispatch_async(dispatch_get_main_queue(), {
                        completion(nil, nil)
                    })
                }
            })
            
            currentTask?.resume()
        }
    }
    
    public func cancelPrevoiusFetches() {
        currentTask?.cancel()
    }
    
}
