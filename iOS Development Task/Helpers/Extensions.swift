//
//  Extensions.swift
//  iOS Development Task
//
//  Created by Martin Markov on 9/7/16.
//  Copyright © 2016 Appolica. All rights reserved.
//

import Foundation
import UIKit

extension NSDate {
    
    class func dateFromString(dateString: String) -> NSDate? {
        let dateFormatter = NSDateFormatter()
        
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
        
        return dateFormatter.dateFromString(dateString)
    }
    
    func displayFormat() -> String {
        let formatter = NSDateFormatter()
        
        formatter.dateStyle = .LongStyle
        
        return formatter.stringFromDate(self)
    }
}

extension String {
    
    func queryEncode() -> String {
        let array = componentsSeparatedByCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
            .filter { $0 != "" }
        
        return array.joinWithSeparator("+")
    }
    
}

extension UIImageView {
    
    func setImageFromUrl(urlString: String) {
        if let url = NSURL(string: urlString) {
            NSURLSession.sharedSession().dataTaskWithURL(url, completionHandler: { (data, response, error) in
                if let data = data {
                    dispatch_async(dispatch_get_main_queue(), {
                        self.image = UIImage(data: data)
                    })
                }
            }).resume()
        }
    }
    
}

