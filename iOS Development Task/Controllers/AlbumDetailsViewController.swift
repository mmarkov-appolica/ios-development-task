//
//  AlbumDetailsViewController.swift
//  iOS Development Task
//
//  Created by Martin Markov on 9/7/16.
//  Copyright © 2016 Appolica. All rights reserved.
//

import UIKit

class AlbumDetailsViewController: UIViewController {

    @IBOutlet weak var thumbnailImageView: UIImageView! {
        didSet {
            //circle image view
            
            thumbnailImageView.layer.masksToBounds = false
            thumbnailImageView.layer.cornerRadius = thumbnailImageView.frame.height / 2
            thumbnailImageView.clipsToBounds = true
        }
    }
    @IBOutlet weak var trackNameLabel: UILabel!
    @IBOutlet weak var albumNameLabel: UILabel!
    @IBOutlet weak var artistNameLabel: UILabel!
    @IBOutlet weak var releaseDateLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        reloadView(nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    private func reloadView(album: AlbumModel?) {
        if let thumbnailUrl = album?.thumbnailUrl {
            thumbnailImageView.setImageFromUrl(thumbnailUrl)
        } else {
            thumbnailImageView.image = UIImage(named: Image.NoImage)
        }
        trackNameLabel.text = album?.trackName ?? "No track"
        albumNameLabel.text = album?.albumName ?? "No album"
        artistNameLabel.text = album?.artistName ?? "No artist"
        releaseDateLabel.text = album?.releaseDate?.displayFormat() ?? "No released date"
        priceLabel.text = album?.priceLabel ?? "No price"
    }
}


// MARK: - AlbumsListTableViewControllerDelegate
extension AlbumDetailsViewController: AlbumsListTableViewControllerDelegate {
    
    func albumsListController(controller: AlbumsListTableViewController, didSelectAlbum album: AlbumModel) {
        reloadView(album)
    }
    
}
