//
//  AlbumsListTableViewController.swift
//  iOS Development Task
//
//  Created by Martin Markov on 9/7/16.
//  Copyright © 2016 Appolica. All rights reserved.
//

import UIKit

protocol AlbumsListTableViewControllerDelegate: class {
    func albumsListController(controller: AlbumsListTableViewController, didSelectAlbum album: AlbumModel)
}

class AlbumsListTableViewController: UITableViewController {
    
    private var albumCellIdentifier = "AlbumCell"
    private var fetcher = AlbumsFetcher()
    private var albums: [AlbumModel] = [] {
        didSet {
            tableView.reloadData()
        }
    }
    
    weak var delegate: AlbumsListTableViewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        clearsSelectionOnViewWillAppear = false
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    private func loadData(query: String?) {
        fetcher.cancelPrevoiusFetches()
        
        guard let query = query else {
            albums = []
            
            return
        }
        
        if query.characters.count != 0 {
            fetcher.getAlbums(query) { (responseAlbums, error) in
                // show the error if the task is not canceled
                
                if let error = error where error.code != NSURLErrorCancelled {
                    let alert = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: .Alert)
                    let okAction = UIAlertAction(title: "OK", style: .Default, handler: nil)
                    
                    alert.addAction(okAction)
                    
                    self.presentViewController(alert, animated: true, completion: nil)
                } else {
                    self.albums = responseAlbums ?? []
                }
            }
        } else {
            albums = []
        }
    }
    
    // MARK: - UITableViewDataSource

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return albums.count
    }

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier(albumCellIdentifier, forIndexPath: indexPath)
        let album = albums[indexPath.row]
        
        cell.detailTextLabel?.text = album.artistName ?? "No artist"
        cell.textLabel?.text = album.trackName ?? "No album"
        cell.imageView?.image = UIImage(named: Image.NoImage)
        if let thumbnailUrl = album.thumbnailUrl {
            cell.imageView?.setImageFromUrl(thumbnailUrl)
        }

        return cell
    }
    
    // MARK: - UITableViewDelegate
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let album = albums[indexPath.row]
        
        delegate?.albumsListController(self, didSelectAlbum: album)
        
        if let detailViewController = delegate as? AlbumDetailsViewController {
            splitViewController?.showDetailViewController(detailViewController, sender: self)
        }
    }
    
}

extension AlbumsListTableViewController: UISearchBarDelegate {
    
    func searchBar(searchBar: UISearchBar, textDidChange searchText: String) {
        loadData(searchText)
    }
    
    func searchBarSearchButtonClicked(searchBar: UISearchBar) {
        tableView.endEditing(true)
    }
    
}
